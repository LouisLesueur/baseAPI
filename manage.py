from flask.cli import FlaskGroup

from api.wsgi import app
from api.extensions import db


# Special subclass of the :class:`AppGroup` group that supports loading more commands from the configured Flask app.
cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    # All the model have to be imported before creating the database to be accounted for
    # This is for dev and test purpose only. Please do true migration for production

    from api.modules.users.models import User

    db.drop_all()
    db.create_all()
    db.session.commit()


if __name__ == "__main__":
    cli()
