import os


class BaseConfig(object):
    SECRET_KEY = 'this-is-not-secret-change-it'

    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

    # POSTGRESQL
    DB_USER = 'api_user'
    DB_PASSWORD = 'api_root'
    DB_NAME = 'api_dev'
    DB_HOST = 'db'
    DB_PORT = 5432
    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{host}:{port}/{name}'.format(
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT,
        name=DB_NAME,
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEBUG = False
    ERROR_404_HELP = False

    ENABLED_MODULES = (
        'users',
    )

    SWAGGER_UI_JSONEDITOR = True
    SWAGGER_UI_OAUTH_CLIENT_ID = 'documentation'
    SWAGGER_UI_OAUTH_REALM = "Authentication for Flask-RESTplus Example server documentation"
    SWAGGER_UI_OAUTH_APP_NAME = "Flask-RESTplus Example server documentation"


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    FLASK_ENV = 'development'


class TestingConfig(BaseConfig):
    TESTING = True
    FLASK_ENV = 'testing'

    # Use in-memory SQLite database for testing
    SQLALCHEMY_DATABASE_URI = 'sqlite://'