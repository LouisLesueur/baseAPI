"""
Pytest + sqlalchemy patch
-------------------------
Following lines will rollback the database after each tests to ensure a clean state before testing
"""
import pytest
from flask_login import login_manager

from api import create_app
from api.modules.users.models import User


@pytest.fixture
def flask_app():
    app = create_app("testing")
    from api.extensions import db

    with app.app_context():
        db.create_all()
        yield app
        db.session.close()
        db.drop_all()


@pytest.fixture
def db(flask_app):
    """
    Drop the database after each tests.
    Add the db argument to your tests to obtain a clean database for your current test
    """

    from api.extensions import db as db_instance
    with flask_app.app_context():
        db_instance.create_all()
        yield db_instance
        db_instance.session.close()
        db_instance.drop_all()


@pytest.fixture
def flask_app_client(flask_app):
    return flask_app.test_client()
