"""
User Login Tests
-----------------------
"""

import json

from tests.fixture_factory import create_user


def test_login_user_ok(flask_app_client, db):
	user_data = {
		'email': 'my_email@gmail.com',
		'password': 'my_email'
	}

	new_user = create_user(db, **user_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps(user_data),
		content_type='application/json'
	)

	assert resp.status_code == 200


def test_login_user_failed_no_email(flask_app_client, db):
	user_data = {
		'email': 'my_email@gmail.com',
		'password': 'my_email'
	}

	new_user = create_user(db, **user_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps({'password': 'my_email'}),
		content_type='application/json'
	)

	assert resp.status_code == 400
	assert resp.json == {
		'errors':
			{
				'email': 'Missing required parameter in the JSON body or the post body or the query string'
			},
		'message': 'Input payload validation failed'
	}


def test_login_user_failed_no_password(flask_app_client, db):
	user_data = {
		'email': 'my_email@gmail.com',
		'password': 'my_email'
	}

	new_user = create_user(db, **user_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps({'email': 'my_email@gmail.com'}),
		content_type='application/json'
	)

	assert resp.status_code == 400
	assert resp.json == {
		'errors':
			{
				'password': 'Missing required parameter in the JSON body or the post body or the query string'
			},
		'message': 'Input payload validation failed'
	}


def test_login_user_failed_bad_password(flask_app_client, db):
	user_data = {
		'email': 'my_email@gmail.com',
		'password': 'my_email'
	}

	new_user = create_user(db, **user_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps({'email': 'my_email@gmail.com', 'password': 'badpassword'}),
		content_type='application/json'
	)

	assert resp.status_code == 400
	assert resp.json == {'type': 'INVALID_REQUEST', 'message': 'Bad email or password.'}
