"""
User Registration Tests
-----------------------
"""
import json

from tests.fixture_factory import create_user


def test_register_user_ok(flask_app_client, db):
    user_data = {
        'email': 'my_email@gmail.com',
        'password': 'my_email@gmail.com'
    }

    resp = flask_app_client.post(
        "/user/",
        data=json.dumps(user_data),
        content_type='application/json'
    )

    assert resp.status_code == 201
    assert resp.json == {
        'id': 1,
        'email': 'my_email@gmail.com',
        'active': True
    }


def test_register_user_ok_check_database(flask_app_client, db):
    user_data = {
        'email': 'my_email@gmail.com',
        'password': 'my_email@gmail.com'
    }

    resp = flask_app_client.post(
        "/user/",
        data=json.dumps(user_data),
        content_type='application/json'
    )

    assert resp.status_code == 201

    user = db.engine.execute("SELECT * FROM users WHERE users.id = 1").first()
    assert user.id == 1
    assert user.email == "my_email@gmail.com"
    assert user.active == True


def test_register_user_ko_email_already_exists(flask_app_client, db):
    create_user(db, email="silverster.stalone@gmail.com", password="ThisIsNotMyWar")
    user_data = {
        'email': 'silverster.stalone@gmail.com',
        'password': 'my_email@gmail.com'
    }

    resp = flask_app_client.post(
        "/user/",
        data=json.dumps(user_data),
        content_type='application/json'
    )

    assert resp.status_code == 409
    assert resp.json == {
        "type": "USER_ALREADY_EXISTS",
        "message": "Email already exists."
    }
