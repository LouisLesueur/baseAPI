"""
User Put Tests
-----------------------
"""
import json

from tests.fixture_factory import create_user


def test_put_user_email_ok(flask_app_client, db):
	silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
	silverster = create_user(db, **silverster_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps(silverster_data),
		content_type='application/json'
	)
	assert resp.status_code == 200

	put_data = {'email': 'jasonStatam@gmail.com'}

	resp = flask_app_client.put(
		"/user/",
		data=json.dumps(put_data),
		content_type='application/json'
	)

	assert resp.status_code == 200
	assert resp.json == {
		'id': 1,
		'email': 'jasonStatam@gmail.com',
		'active': True
	}


def test_put_user_password_ok(flask_app_client, db):
	silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
	silverster = create_user(db, **silverster_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps(silverster_data),
		content_type='application/json'
	)
	assert resp.status_code == 200

	put_data = {'password': 'jasonStatam'}

	resp = flask_app_client.put(
		"/user/",
		data=json.dumps(put_data),
		content_type='application/json'
	)

	assert resp.status_code == 200
	assert resp.json == {
		'id': 1,
		'email': 'silverster.stalone@gmail.com',
		'active': True
	}


def test_put_user_password_and_email_ok(flask_app_client, db):
	silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
	silverster = create_user(db, **silverster_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps(silverster_data),
		content_type='application/json'
	)
	assert resp.status_code == 200

	put_data = {'email': 'jasonStatam@gmail.com', 'password': 'jasonStatam'}

	resp = flask_app_client.put(
		"/user/",
		data=json.dumps(put_data),
		content_type='application/json'
	)

	assert resp.status_code == 200
	assert resp.json == {
		'id': 1,
		'email': 'jasonStatam@gmail.com',
		'active': True
	}


def test_put_user_email_failed(flask_app_client, db):
	silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
	silverster = create_user(db, **silverster_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps(silverster_data),
		content_type='application/json'
	)
	assert resp.status_code == 200

	put_data = {'email': 'jasonStatam'}

	resp = flask_app_client.put(
		"/user/",
		data=json.dumps(put_data),
		content_type='application/json'
	)

	assert resp.status_code == 400
	assert resp.json == {'errors': {'email': 'Invalid email'}, 'message': 'Input payload validation failed'}
