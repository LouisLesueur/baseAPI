"""
User Logout Tests
-----------------------
"""
import json

from tests.fixture_factory import create_user


def test_logout_user_ok(flask_app_client, db):
	silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
	silverster = create_user(db, **silverster_data)
	resp = flask_app_client.post(
		"/user/login",
		data=json.dumps(silverster_data),
		content_type='application/json'
	)
	assert resp.status_code == 200

	resp = flask_app_client.get(
		"/user/logout",
	)

	assert resp.status_code == 204

	put_data = {'email': 'jasonStatam@gmail.com'}

	resp = flask_app_client.put(
		"/user/",
		data=json.dumps(put_data),
		content_type='application/json'
	)

	assert resp.status_code == 401
	assert resp.json == {
		'message': "The server could not verify that you are authorized to access the URL requested."
					" You either supplied the wrong credentials (e.g. a bad password),"
					" or your browser doesn't understand how to supply the credentials required."
	}
