"""
User get by id Tests
-----------------------
"""
import json

from tests.fixture_factory import create_user


def test_get_user_by_id_ok(flask_app_client, db):
    silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
    silverster = create_user(db, **silverster_data)
    resp = flask_app_client.post(
        "/user/login",
        data=json.dumps(silverster_data),
        content_type='application/json'
    )
    assert resp.status_code == 200

    resp = flask_app_client.get("/user/1")

    assert resp.status_code == 200
    assert resp.json == {
        'id': 1,
        'email': 'silverster.stalone@gmail.com',
        'active': True
    }


def test_get_user_by_id_ko_user_not_found(flask_app_client, db):
    silverster_data = dict(email="silverster.stalone@gmail.com", password="ThisWasNotMyWar")
    silverster = create_user(db, **silverster_data)
    resp = flask_app_client.post(
        "/user/login",
        data=json.dumps(silverster_data),
        content_type='application/json'
    )
    assert resp.status_code == 200

    resp = flask_app_client.get("/user/42069")

    assert resp.status_code == 404
    assert resp.json == {
        'message': 'User not found.',
        'type': 'USER_NOT_FOUND'
    }
