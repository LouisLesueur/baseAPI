"""
Fixture factory
---------------
Will create the models in database for you
"""
from tests.conftest import db
from api.modules.users.models import User


def create_user(db=db, **kwargs):
    new_user = User(**kwargs)
    db.session.add(new_user)
    db.session.commit()
    return new_user
