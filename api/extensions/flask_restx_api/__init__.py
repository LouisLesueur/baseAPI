# encoding: utf-8
"""
API extension
=============
"""

from flask_restx import Api


api = Api(
    version='1.0',
    title="baseAPI",
    description=(
        "* Self-documented RESTful API server;\n"
    ),
)
