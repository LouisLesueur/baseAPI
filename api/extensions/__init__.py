# encoding: utf-8
"""
Extensions setup
================
Extensions provide access to common resources of the application.
"""
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from api.extensions.flask_restx_api import api


db = SQLAlchemy()
login = LoginManager()


def init_app(app):
    """
    Application extensions initialization.
    """
    for extension in (
            db,
            api,
            login,
    ):
        extension.init_app(app)
