# encoding: utf-8
"""
Wsgi
----
"""
from api import create_app

app = create_app()
