"""
User database models
--------------------
"""

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from api.extensions import db, login


class User(UserMixin, db.Model):
    """
    User model
    ==========
    id: Simple identifier for an easy database access to users
    email: Email of the user will serve during authentication
    hashed_password: Hash of the original password
    active: True if the user have been active lately [WiP]
    """

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)  # pylint: disable=invalid-name
    email = db.Column(db.String(256), unique=True, nullable=False)
    hashed_password = db.Column(db.String(256), nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)

    def __init__(self, email, password):
        self.email = email
        self.set_password(password)

    def set_password(self, password):
        self.hashed_password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.hashed_password, password)

    @classmethod
    def get_user_by_id(cls, user_id):
        """
        Args:
            user_id (int) - User id you are trying to find
        Returns:
            user (User) - if there is a user with a specified id, None otherwise.
        """
        return cls.query.filter(User.id == user_id).first()

    @classmethod
    def get_user_by_email(cls, email):
        """
        Args:
            email (str) - User email you are trying to find
        Returns:
            user (User) - if there is a user with a specified email, None otherwise.
        """
        return cls.query.filter(User.email == email).first()


@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
