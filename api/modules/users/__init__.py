# encoding: utf-8
"""
Users module
============
"""

from api.extensions.flask_restx_api import api


def init_app(app, **kwargs):
    # pylint: disable=unused-argument,unused-variable,unused-import,import-outside-toplevel
    """
    Init users module.
    """
    from . import models, endpoints
    api.add_namespace(endpoints.user_namespace)
