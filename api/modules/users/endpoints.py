# encoding: utf-8
"""
RESTful API User resources
--------------------------
"""
from flask_restx import Resource, Namespace, reqparse, fields
from flask_login import current_user, login_user, logout_user, login_required

from api.extensions import db
from api.modules.utilities.email_verifier import email_validator
from api.modules.users.models import User


user_namespace = Namespace('user', description="User management")

user_schema = user_namespace.model('User', {
    'id': fields.Integer,
    'email': fields.String,
    'active': fields.Boolean
})

auth_parser = reqparse.RequestParser()
auth_parser.add_argument('email', type=email_validator, required=True)
auth_parser.add_argument('password', type=str, required=True)

modify_user_parser = reqparse.RequestParser()
modify_user_parser.add_argument('email', type=email_validator, required=False)
modify_user_parser.add_argument('password', type=str, required=False)


@user_namespace.route('/')
class UserManage(Resource):
    """
    Manage user resource
    """
    @user_namespace.doc('Create a user')
    @user_namespace.expect(auth_parser, validate=True)
    @user_namespace.response(409, 'Email already exists.')
    @user_namespace.marshal_with(user_schema, code=201)
    def post(self):
        """
        Register a new user
        """
        data = auth_parser.parse_args(strict=True)
        if User.get_user_by_email(data['email']):
            user_namespace.abort(409, 'Email already exists.', type="USER_ALREADY_EXISTS")

        new_user = User(**data)
        db.session.add(new_user)
        db.session.commit()

        login_user(new_user)

        return new_user, 201

    @login_required
    @user_namespace.doc('Create a user')
    @user_namespace.expect(modify_user_parser, validate=True)
    @user_namespace.response(400, 'Email already exists.')
    @user_namespace.marshal_with(user_schema, code=201)
    def put(self):
        """
        Modify a user
        """
        user = current_user
        data = modify_user_parser.parse_args(strict=True)
        if User.get_user_by_email(data['email']):
            user_namespace.abort(400, 'Email already exists.', type="CANNOT_CHANGE_USER")
        if data.get('password', None):
            user.set_password(data.pop('password'))

        for key, value in data.items():
            if value:
                setattr(user, key, value)

        db.session.add(user)
        db.session.commit()

        return user, 200


@user_namespace.route('/login')
class UserLogin(Resource):
    """
    Login user resource
    """
    @user_namespace.doc('Login as a user')
    @user_namespace.expect(auth_parser, validate=True)
    @user_namespace.response(400, 'Bad email or password')
    @user_namespace.marshal_with(user_schema, code=201)
    def post(self):
        """
        Login a user
        """
        if current_user.is_authenticated:
            return "Already logged in", 204

        data = auth_parser.parse_args(strict=True)

        user = User.get_user_by_email(data.get('email'))
        if user is None or not user.check_password(data.get('password')):
            user_namespace.abort(400, 'Bad email or password.', type="INVALID_REQUEST")

        login_user(user)

        return user, 200


@user_namespace.route('/logout')
class UserLogout(Resource):
    """
    Logout user resource
    """
    @login_required
    @user_namespace.doc('Logout as a user')
    @user_namespace.response(204, '')
    def get(self):
        """
        Logout a user
        """
        logout_user()
        return '', 204


@user_namespace.route('/<int:user_id>')
@user_namespace.response(404, 'User not found.')
class UserById(Resource):
    """
    Manipulation with user by id
    """
    @login_required
    @user_namespace.doc('Get a user by id')
    @user_namespace.marshal_with(user_schema, skip_none=True, code=200)
    def get(self, user_id):
        """
        Get a user by id
        """
        user = User.get_user_by_id(user_id)
        if not user:
            user_namespace.abort(404, 'User not found.', type="USER_NOT_FOUND")
        return user
