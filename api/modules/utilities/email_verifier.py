from validate_email import validate_email


def email_validator(email):
	if validate_email(email_address=email, check_smtp=False) == False or len(email) > 255:
		raise ValueError("Invalid email")
	return email
